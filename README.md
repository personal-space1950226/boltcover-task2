# Node.js CI/CD Pipeline with GitLab and Terraform

This repository contains a simple Node.js application with a CI/CD pipeline that builds, tests, and deploys the application using GitLab CI/CD and Terraform to AWS ECS.

## Project Structure


```
.
├── README.md
├── app
│   ├── Dockerfile
│   ├── index.js
│   ├── package.json
│   └── test
│       └── test.js
├── ci-cd
│   └── .gitlab-ci.yml
├── terraform
│   ├── main.tf
│   ├── outputs.tf
│   └── variables.tf
```


## Setup Instructions

### 1. Node.js Application

The Node.js application is located in the `app` directory. It includes:

- **`app/package.json`**: Defines the application's dependencies and scripts.
- **`app/index.js`**: Sets up a simple Express server.
- **`app/test/test.js`**: Contains a test for the Express server using Mocha and Chai.

### 2. Dockerfile

The `app/Dockerfile` is used to build a Docker image for the Node.js application.

### 3. CI/CD Pipeline Configuration

The CI/CD pipeline is configured in the `ci-cd/.gitlab-ci.yml` file.

#### `.gitlab-ci.yml`

```yaml
stages:
  - test
  - build
  - deploy

variables:
  DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

test:
  image: node:14
  stage: test
  script:
    - cd app
    - npm install
    - npm test

build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  script:
    - cd app
    - docker build -t $DOCKER_IMAGE .
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker push $DOCKER_IMAGE

deploy:
  image:
    name: hashicorp/terraform:light
    entrypoint: [""]
  stage: deploy
  before_script:
    - cd terraform
    - terraform init
  script:
    - terraform validate
    - terraform apply -var="docker_image=$DOCKER_IMAGE" -auto-approve
  only:
    - main
```

### 4. Terraform Configuration

The Terraform configuration files are located in the `terraform` directory:

- **`terraform/main.tf`**: Contains the main Terraform configuration.
- **`terraform/variables.tf`**: Defines input variables.
- **`terraform/outputs.tf`**: Defines output variables.

### Potential Challenges and Solutions

#### Challenge: Terraform `sh` Command Not Found

Error:
```
Terraform has no command named "sh". Did you mean "show"?"
```

**Solution**:
Override the entrypoint in the Terraform image to access the shell.

```yaml
deploy:
  image:
    name: hashicorp/terraform:light
    entrypoint: [""]
  stage: deploy
  before_script:
    - cd terraform
    - terraform init
  script:
    - terraform validate
    - terraform apply -var="docker_image=$DOCKER_IMAGE" -auto-approve
  only:
    - main
```

#### Challenge: Missing `chai` Module

Error:
```
Error: Cannot find module 'chai'
```

**Solution**:
Ensure that `chai` and `chai-http` are included in the `package.json` and properly installed.

#### Challenge: Passing Terraform Variables

Error:
```
Error: No value for required variable
```

**Solution**:
Pass the required variable `docker_image` during the `terraform apply` command.

```yaml
script:
  - terraform apply -var="docker_image=$DOCKER_IMAGE" -auto-approve
```

