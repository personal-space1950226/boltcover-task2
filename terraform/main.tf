provider "aws" {
  region = "us-east-1"
}

# Data source to get current VPCs
data "aws_vpcs" "all" {}

resource "null_resource" "vpc_exists" {
  provisioner "local-exec" {
    command = "echo '${data.aws_vpcs.all.ids}' | grep -q '${var.vpc_cidr_block}'"
  }
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr_block

  lifecycle {
    create_before_destroy = true
    prevent_destroy = true
  }

  depends_on = [null_resource.vpc_exists]
}

resource "aws_subnet" "main" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  lifecycle {
    create_before_destroy = true
    prevent_destroy = true
  }

  depends_on = [aws_vpc.main]
}

resource "aws_security_group" "allow_all" {
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
    prevent_destroy = true
  }

  depends_on = [aws_vpc.main]
}

resource "aws_ecs_cluster" "main" {
  name = "main-cluster"

  lifecycle {
    create_before_destroy = true
    prevent_destroy = true
  }
}

resource "aws_ecs_task_definition" "main" {
  family                   = "task-definition"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256"
  memory                   = "512"

  container_definitions = jsonencode([
    {
      name      = "node-app",
      image     = var.docker_image,
      essential = true,
      portMappings = [
        {
          containerPort = 3000
          hostPort      = 3000
        }
      ]
    }
  ])

  lifecycle {
    create_before_destroy = true
    prevent_destroy = true
  }
}

resource "null_resource" "ecs_service" {
  provisioner "local-exec" {
    command = <<EOT
      aws ecs describe-services --cluster ${aws_ecs_cluster.main.id} --services main-service || \
      aws ecs create-service --cluster ${aws_ecs_cluster.main.id} --service-name main-service --task-definition ${aws_ecs_task_definition.main.arn} --desired-count 1 --launch-type FARGATE --network-configuration "awsvpcConfiguration={subnets=[${aws_subnet.main.id}],securityGroups=[${aws_security_group.allow_all.id}],assignPublicIp=ENABLED}"
    EOT
  }

  triggers = {
    cluster_id      = aws_ecs_cluster.main.id
    task_definition = aws_ecs_task_definition.main.arn
  }
}